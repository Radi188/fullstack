/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View, NativeModules, Image} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';

// const {ImageLabelingModule} = NativeModules;

// const eventEmitter = new NativeEventEmitter(ImageLabelingModule);
// console.log(ImageLabelingModule);
// console.log(ImageLabelingModule.createCalendarEvent(res => console.log(res)));

const App = () => {
  const [path, setPath] = useState('');
  // const [data, setData] = useState();
  // useEffect(() => {
  //   eventEmitter.addListener('EventCount', eventCount => {
  //     console.log(eventCount);
  //   });
  //   return () => {
  //     eventEmitter.removeAllListeners('EventCount');
  //   };
  // }, []);

  // const imageLabeling = async url => {
  //   return await ImageLabelingModule.createImageLabelingEvent(url);
  // };

  // const createCalendarPromise = async () => {
  //   try {
  //     const result = await ImageLabelingModule.createCalendarPromise();
  //     console.log(result);
  //   } catch (e) {
  //     console.log(e);
  //   }
  // };

  const openGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: false,
    }).then(async image => {
      console.log(image);
      setPath(image.path);
      // try {
      //   const response = await imageLabeling(image.path);
      //   setData(response);
      //   console.log(response);
      // } catch (error) {
      //   console.log(error);
      // }
    });
  };

  const openCamera = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: false,
    }).then(async image => {
      console.log(image);
      setPath(image.path);
      // try {
      //   const response = await imageLabeling(image.path);
      //   console.log(response);
      //   setData(response);
      // } catch (error) {
      //   console.log('Catch Error', error);
      // }
    });
  };
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Image
        source={{
          uri: path,
        }}
        style={{height: 300, width: 300}}
        imageStyle={{borderRadius: 15}}
      />
      <View
        style={{
          marginTop: 30,
        }}>
        {/* {data ? (
          <View>
            {data.block.map(item => (
              <View
                style={{
                  borderColor: '#A7A7A7',
                  borderWidth: 1,
                }}>
                <Text>Label : {item.text}</Text>
                <Text>
                  Confidence :{Math.round(item.confidence * 100) / 100}%
                </Text>
              </View>
            ))}
          </View>
        ) : null} */}
      </View>
      <View
        style={{
          marginTop: 20,
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          style={{
            height: 50,
            width: '35%',
            backgroundColor: 'skyblue',
            borderRadius: 20,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}
          onPress={openGallery}>
          <Text style={{fontSize: 16}}>Open gallery</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            height: 50,
            width: '35%',
            backgroundColor: 'skyblue',
            borderRadius: 20,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
          }}
          onPress={openCamera}>
          <Text style={{fontSize: 16}}>Open camera</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default App;
