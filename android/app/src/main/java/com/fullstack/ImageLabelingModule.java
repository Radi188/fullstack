package com.fullstack; // replace your-apps-package-name with your app’s package name
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import android.graphics.Rect;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.label.ImageLabel;
import com.google.mlkit.vision.label.ImageLabeler;
import com.google.mlkit.vision.label.ImageLabeling;
import com.google.mlkit.vision.label.defaults.ImageLabelerOptions;

public class ImageLabelingModule extends ReactContextBaseJavaModule {

    private int eventCount = 0;
    ImageLabelingModule(ReactApplicationContext context) {
        super(context);
    }
    @Override
    public String getName() {
        return "ImageLabelingModule";
    }
    @ReactMethod
    public void createImageLabelingEvent(String url, Promise promise) {
        Log.d("ImageLabelingModule", "url: " + url );
        Uri uri = Uri.parse(url);
        // To use default options:
        InputImage image;
        try {
            image = InputImage.fromFilePath(getReactApplicationContext(), uri);
            ImageLabeler labeler = ImageLabeling.getClient(ImageLabelerOptions.DEFAULT_OPTIONS);
            labeler.process(image)
                    .addOnSuccessListener(new OnSuccessListener<List<ImageLabel>>() {
                        @Override
                        public void onSuccess(List<ImageLabel> labels) {
                            WritableMap response = Arguments.createMap();

                            WritableArray blocks = Arguments.createArray();
                            for (ImageLabel label : labels) {
                                WritableMap blockObject = Arguments.createMap();
                                blockObject.putString("text" , label.getText());
                                blockObject.putDouble("confidence", label.getConfidence());
                                blockObject.putInt("index", label.getIndex());

                                blocks.pushMap(blockObject);
                            }
                            response.putArray("block",blocks);
                            promise.resolve(response);
                        }

                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            promise.reject("Image Labeling seem error ", e);
                        }
                    });

        } catch (IOException e) {
            e.printStackTrace();
            promise.reject("Image Labeling Error", e);
        }
    }
}
